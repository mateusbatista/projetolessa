package br.com.teatro.bean;

public class Fantasia {
	private int idFantasia;
	private String nome;
	private String descricao;
	private int qntPecas;
	private int idTema;
	private boolean possuiPerolas;
	
	public int getIdFantasia() {
		return idFantasia;
	}
	public void setIdFantasia(int idFantasia) {
		this.idFantasia = idFantasia;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getQntPecas() {
		return qntPecas;
	}
	public void setQntPecas(int qntPecas) {
		this.qntPecas = qntPecas;
	}
	public int getIdTema() {
		return idTema;
	}
	public void setIdTema(int idTema) {
		this.idTema = idTema;
	}
	public boolean isPossuiPerolas() {
		return possuiPerolas;
	}
	public void setPossuiPerolas(boolean possuiPerolas) {
		this.possuiPerolas = possuiPerolas;
	}
	

}
