package br.com.teatro.bean;

public class Maquiagem {
	private String nome;
	private String descricao;
	private String listaMaquiagem;
	private int idTema;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getListaMaquiagem() {
		return listaMaquiagem;
	}
	public void setListaMaquiagem(String listaMaquiagem) {
		this.listaMaquiagem = listaMaquiagem;
	}
	public int getIdTema() {
		return idTema;
	}
	public void setIdTema(int idTema) {
		this.idTema = idTema;
	}
	
	
}
