package br.com.teatro.bean;

public class Cenario {
	private int idCenario;
	private String nome;
	private int qntItens;
	private String descricaoItens;
	private int idTema;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getQntItens() {
		return qntItens;
	}
	public void setQntItens(int qntItens) {
		this.qntItens = qntItens;
	}
	public String getDescricaoItens() {
		return descricaoItens;
	}
	public void setDescricaoItens(String descricaoItens) {
		this.descricaoItens = descricaoItens;
	}
	public int getIdTema() {
		return idTema;
	}
	public void setIdTema(int idTema) {
		this.idTema = idTema;
	}
	public int getIdCenario() {
		return idCenario;
	}
	public void setIdCenario(int idCenario) {
		this.idCenario = idCenario;
	}
}
