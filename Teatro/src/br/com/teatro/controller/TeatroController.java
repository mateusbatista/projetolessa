package br.com.teatro.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.teatro.bean.Cenario;
import br.com.teatro.bean.Fantasia;
import br.com.teatro.bean.Item;
import br.com.teatro.bean.Maquiagem;
import br.com.teatro.bean.Tema;
import br.com.teatro.dao.CenarioDao;
import br.com.teatro.dao.DaoFactory;
import br.com.teatro.dao.DaoFactory.Fonte;
import br.com.teatro.dao.FantasiaDao;
import br.com.teatro.dao.ItemDao;
import br.com.teatro.dao.MaquiagemDao;
import br.com.teatro.dao.TemaDao;

@Controller
public class TeatroController {
	
	private DaoFactory dao = DaoFactory.getDaoFactory(Fonte.POSTGRES);
	private TemaDao daoTema= dao.getTemaDao();
	private CenarioDao daoCenario = dao.getCenarioDao();
	private FantasiaDao daoFantasia = dao.getFantasiaDao();
	private MaquiagemDao daoMaquiagem = dao.getMaquiagemDao();
	private ItemDao daoItem = dao.getItemDao();
	
	@RequestMapping("novoTema")
	public String novoTema(Tema novoTema) {
		boolean rs = false;
		
		if (novoTema != null){
			rs = daoTema.novoTema(novoTema);
		}
		
		if (rs){
			return "tema";
		}else{
			return "erro";
		}	
	}
	
	@RequestMapping("novoCenario")
	public String novoCenario(Cenario novoCenario,HttpServletRequest request){
		boolean rs = false;
		
		if (novoCenario != null){
			novoCenario.setIdTema(Integer.parseInt(request.getParameter("idTema")));
			rs = daoCenario.novoCenario(novoCenario);
		}
		
		if (rs){
			return "redirect:cenario";
		}else{
			return "erro";
		}
	}
	
	@RequestMapping("novaFantasia")
	public String novaFantasia(Fantasia novaFantasia, HttpServletRequest request){
		boolean rs = false;
		
		if(novaFantasia != null){
			rs = daoFantasia.novaFantasia(novaFantasia);
		}
		if (rs){
			return "redirect:fantasia";
		}else{
			return "erro";
		}
	}
	
	@RequestMapping("novaMaquiagem")
	public String novaMaquiagem(Maquiagem novaMaquiagem, HttpServletRequest request){
		boolean rs = false;
		
		if(novaMaquiagem != null){
			rs = daoMaquiagem.novaMaquiagem(novaMaquiagem);
		}
		if (rs){
			return "redirect:maquiagem";
		}else{
			return "erro";
		}
	}
	
	@RequestMapping("index")
	public String menuIndex(){
		return "index";
	}

	@RequestMapping("tema")
	public String menuTema(){
		return "tema";
	}
		
	@RequestMapping("cenario")
	public String menuCenario(Model model){
		
		ArrayList<Tema> listaTemas = daoTema.getListaTemas();
		
		if (listaTemas != null){
			model.addAttribute("listaTemas", listaTemas);
		}
		
		return "cenario";
	}
	
	@RequestMapping("fantasia")
	public String menuFantasia(Model model){
		
		ArrayList<Tema> listaTemas = daoTema.getListaTemas();
		
		if (listaTemas != null){
			model.addAttribute("listaTemas", listaTemas);
		}
		
		return "fantasia";
	}
	
	@RequestMapping("maquiagem")
	public String menuMaquiagem(Model model){
		
		ArrayList<Tema> listaTemas = daoTema.getListaTemas();
		
		if (listaTemas != null){
			model.addAttribute("listaTemas", listaTemas);
		}
		
		return "maquiagem";
	}
	
	@RequestMapping("relatorioTema")
	public String menuRelatorioTema(Model model, HttpServletRequest request){
		
		String tema = request.getParameter("idTema");
		
		ArrayList<Item> listaItens = null;
		
		if (tema != null && !tema.isEmpty()){
			int idTema = Integer.parseInt(tema);			
			listaItens = daoItem.getListaItens(idTema);
			
		}else{
			listaItens = daoItem.getListaItens();
		}
		
		
		ArrayList<Tema> listaTemas = daoTema.getListaTemas();
				
		if (listaTemas != null){
			model.addAttribute("listaTemas", listaTemas);
		}
		
		if(listaItens != null){
			model.addAttribute("listaItens", listaItens);
		}
		
		return "relatorio-tema";
	}
	
	@RequestMapping("relatorioTipo")
	public String menuRelatorioTipo(Model model, HttpServletRequest request){
		
		String tipo = request.getParameter("idTipo");
		
		ArrayList<Item> listaItens = null;
		
		if (tipo != null && !tipo.isEmpty()){
			int idTipo = Integer.parseInt(tipo);			
			listaItens = daoItem.getListaItensTipo(idTipo);
			
		}else{
			listaItens = daoItem.getListaItensTipo(0);
		}
		
		if(listaItens != null){
			model.addAttribute("listaItens", listaItens);
		}
		
		return "relatorio-tipo";
	}


}
