package br.com.teatro.controller;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.teatro.bean.Usuario;
import br.com.teatro.dao.DaoFactory;
import br.com.teatro.dao.DaoFactory.Fonte;
import br.com.teatro.dao.UsuarioDao;

@Controller
public class LoginController {

	private DaoFactory dao = DaoFactory.getDaoFactory(Fonte.POSTGRES);
	private UsuarioDao daoUsuario = dao.getUsuarioDao();

	@RequestMapping("login")
	public String loginForm() {
		return "formulario-login";
	}

	@RequestMapping("efetuaLogin")
	public String efetuaLogin(Usuario usuario, HttpSession session) {
		Usuario usuarioLogin = daoUsuario.getUsuario(usuario);
		if (usuarioLogin != null) {

			String login = usuarioLogin.getLogin();

			login = WordUtils.capitalize(login);

			usuarioLogin.setLogin(login);

			session.setAttribute("usuarioLogado", usuarioLogin);
			
			return "index";	

		}
		return "redirect:login";
	}

	@RequestMapping("logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:login";
	}

}
