package br.com.teatro.dao;

import java.util.ArrayList;

import br.com.teatro.bean.Maquiagem;

public interface MaquiagemDao {
	public boolean novaMaquiagem(Maquiagem novaMaquiagem);
	public ArrayList<Maquiagem>getListaMaquiagens();
}
