package br.com.teatro.dao;

import java.util.ArrayList;

import br.com.teatro.bean.Fantasia;

public interface FantasiaDao {
	public boolean novaFantasia(Fantasia novaFantasia);
	public ArrayList<Fantasia>getListaFantasias();

}