package br.com.teatro.dao;

import br.com.teatro.dao.postgres.PostgreSQLDaoFactory;

public abstract class DaoFactory {
	public enum Fonte {
		MYSQL, MONGODB, POSTGRES
	};

	public static DaoFactory getDaoFactory(Fonte fonte) {
		if (fonte == Fonte.MYSQL) {
			return null;
		} else if (fonte == Fonte.POSTGRES) {
			return new PostgreSQLDaoFactory();
		}

		return null;
	}

	public abstract TemaDao getTemaDao();

	public abstract CenarioDao getCenarioDao();

	public abstract FantasiaDao getFantasiaDao();

	public abstract MaquiagemDao getMaquiagemDao();
	
	public abstract ItemDao getItemDao();
	
	public abstract UsuarioDao getUsuarioDao();

}
