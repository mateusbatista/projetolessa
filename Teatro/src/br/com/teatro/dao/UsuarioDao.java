package br.com.teatro.dao;

import br.com.teatro.bean.Usuario;

public interface UsuarioDao {
	public Usuario getUsuario(Usuario usuario);
	public int adicionaUsuario(Usuario novoUsuario);
	public int alteraUsuario(Usuario usuario);
}
