package br.com.teatro.dao.postgres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PostgreSQLConnectionFactory {
	
	static public Connection getConexao(){		
		try { 
			// Load the JDBC driver 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			// Create a connection to the database 
			String serverName = "127.0.0.1:5432"; 
			String mydatabase = "Teatro"; 
			//jdbc:postgresql://hostname:port/dbname","username", "password")
			String url = "jdbc:postgresql://" + serverName + "/" + mydatabase ;
			String username = "postgres"; 
			String password = "postgres"; 
			return DriverManager.getConnection(url, username, password); 
		} catch (ClassNotFoundException e) { 
			// Could not find the database driver
			System.out.println("N�o encontrou o JDBC");
			e.printStackTrace();
		} catch (SQLException e) { 			
			// Could not connect to the database
			System.out.println("N�o conectou na base");
			e.printStackTrace();
		}

		return null;
	}

	public static void fechaRecursos(Connection conn) {
		try {
			if(conn != null) {
				conn.close();
			}
		}catch(SQLException e) {
			
		}
	}

	public static void fechaRecursos(Connection conn, PreparedStatement ps) {
		try {
			if(ps != null) {
				ps.close();
			}
			fechaRecursos(conn);
		}catch(Exception e) {

		}
	}
	
	public static void fechaRecursos(Connection conn, Statement stmt) {
		try {
			if(stmt != null) {
				stmt.close();
			}
			fechaRecursos(conn);
		}catch(Exception e) {

		}
	}
	
	public static void fechaRecursos(Connection conn, PreparedStatement ps, ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
			fechaRecursos(conn, ps);
		}catch(Exception e) {

		}
	}
	
	public static void fechaRecursos(Connection conn, Statement ps, ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
			fechaRecursos(conn, ps);
		}catch(Exception e) {

		}
	
	}
	
}
