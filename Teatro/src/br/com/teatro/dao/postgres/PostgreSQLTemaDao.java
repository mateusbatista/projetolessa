package br.com.teatro.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.teatro.bean.Tema;
import br.com.teatro.dao.TemaDao;

public class PostgreSQLTemaDao implements TemaDao {

	@Override
	public boolean novoTema(Tema novoTema) {
		
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "INSERT INTO tema (nome, descricao) values (?,?)";
				
				
				ps = conn.prepareStatement(SQL);
				
				ps.setString(1, novoTema.getNome());
				ps.setString(2, novoTema.getDescricao());

				
				int rs = ps.executeUpdate();
				
				if (rs > 0){
					return true;
				}
				
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao gravar tema!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps);
		}		

		return false;
	}

	@Override
	public ArrayList<Tema> getListaTemas() {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
	
		
		ArrayList<Tema> listaTemas = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "SELECT * FROM TEMA";
		
				ps = conn.prepareStatement(SQL);
								
				rs = ps.executeQuery();
				
				listaTemas = new ArrayList<Tema>();
				
				while (rs.next()){
					Tema tema = new Tema();
						
					tema.setIdTema(rs.getInt("id_tema"));
					tema.setNome(rs.getString("nome"));
					tema.setDescricao(rs.getString("descricao"));

					listaTemas.add(tema);
					
				}
				return listaTemas;
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao obter lista de temas!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
		}		
		
		return listaTemas = null;
	}

}
