package br.com.teatro.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.teatro.bean.Maquiagem;
import br.com.teatro.dao.MaquiagemDao;

public class PostgreSQLMaquiagemDao implements MaquiagemDao {

	@Override
	public boolean novaMaquiagem(Maquiagem novaMaquiagem) {
		
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "INSERT INTO maquiagem (nome, id_tema, descricao, lista_maquiagem) values (?,?,?,?)";
				
				
				ps = conn.prepareStatement(SQL);
				
				ps.setString(1, novaMaquiagem.getNome());
				ps.setInt(2, novaMaquiagem.getIdTema());
				ps.setString(4, novaMaquiagem.getDescricao());
				ps.setString(3, novaMaquiagem.getListaMaquiagem());

				
				int rs = ps.executeUpdate();
				
				if (rs > 0){
					return true;
				}
				
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao gravar maquiagem!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps);
		}		

		return false;
	}

	@Override
	public ArrayList<Maquiagem> getListaMaquiagens() {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
	
		
		ArrayList<Maquiagem> listaMaquiagens = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "SELECT * FROM FANTASIA";
		
				ps = conn.prepareStatement(SQL);
								
				rs = ps.executeQuery();
				
				listaMaquiagens = new ArrayList<Maquiagem>();
				
				while (rs.next()){
					Maquiagem maquiagem = new Maquiagem();
						

					listaMaquiagens.add(maquiagem);
					
				}
				return listaMaquiagens;
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao obter lista de temas!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
		}		
		
		return listaMaquiagens = null;
	}

}
