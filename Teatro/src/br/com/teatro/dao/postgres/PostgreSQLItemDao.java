package br.com.teatro.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.teatro.bean.Item;
import br.com.teatro.dao.ItemDao;

public class PostgreSQLItemDao implements ItemDao {

	@Override
	public ArrayList<Item> getListaItens() {
		
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
	
		
		ArrayList<Item> listaItens = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "SELECT id_cenario AS ID,nome, descricao_itens AS DESCRICAO FROM CENARIO "
						+"UNION "
						+"SELECT id_fantasia, nome, descricao FROM FANTASIA "
						+"UNION "
						+"SELECT id_maquiagem, nome, descricao FROM MAQUIAGEM "
						+"ORDER BY NOME";
		
				ps = conn.prepareStatement(SQL);
								
				rs = ps.executeQuery();
				
				listaItens = new ArrayList<Item>();
				
				while (rs.next()){
					Item item = new Item();
					
					item.setNome(rs.getString("nome"));
					item.setId(rs.getInt("ID"));
					item.setDescricao(rs.getString("descricao"));
					
					listaItens.add(item);
					
				}
				return listaItens;
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao obter lista de itens!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
		}		
		
		return listaItens = null;
	}
	
	public ArrayList<Item> getListaItens(int idTema) {
			
			Connection conn = null;
			ResultSet rs = null;
			PreparedStatement ps = null;
		
			
			ArrayList<Item> listaItens = null;
			
			try {
				conn = PostgreSQLConnectionFactory.getConexao();
	
				if (conn != null) {
				
					String SQL = "SELECT id_cenario AS ID,nome, descricao_itens AS DESCRICAO FROM CENARIO WHERE id_tema = ? "
							+"UNION "
							+"SELECT id_fantasia, nome, descricao FROM FANTASIA WHERE id_tema  = ? "
							+"UNION "
							+"SELECT id_maquiagem, nome, descricao FROM MAQUIAGEM WHERE id_tema  = ? "
							+"ORDER BY NOME";
			
					ps = conn.prepareStatement(SQL);
					
					ps.setInt(1, idTema);
					ps.setInt(2, idTema);
					ps.setInt(3, idTema);
									
					rs = ps.executeQuery();
					
					listaItens = new ArrayList<Item>();
					
					while (rs.next()){
						Item item = new Item();
						
						item.setNome(rs.getString("nome"));
						item.setId(rs.getInt("ID"));
						item.setDescricao(rs.getString("descricao"));
						
						listaItens.add(item);
						
					}
					return listaItens;
				}
				
			} catch (SQLException e) {
				System.out.println("Erro ao obter lista de itens!");
				e.printStackTrace();
			} finally {
				PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
			}		
			
			return listaItens = null;
	}

	@Override
	public ArrayList<Item> getListaItensTipo(int idTipo) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
	
		
		ArrayList<Item> listaItens = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
				
				String SQL = "";
				
				if (idTipo == 0){
					SQL = "SELECT id_cenario AS ID,nome, descricao_itens AS DESCRICAO FROM CENARIO "
							+"UNION "
							+"SELECT id_fantasia, nome, descricao FROM FANTASIA "
							+"UNION "
							+"SELECT id_maquiagem, nome, descricao FROM MAQUIAGEM "
							+"ORDER BY NOME";
				}else if(idTipo == 1){
					SQL = "SELECT id_fantasia AS ID, nome, descricao FROM FANTASIA "
							+"UNION "
							+"SELECT id_maquiagem AS ID, nome, descricao FROM MAQUIAGEM "
							+"ORDER BY NOME";
			
				}else if(idTipo == 2){
					SQL = "SELECT id_cenario AS ID,nome, descricao_itens AS DESCRICAO FROM CENARIO  "
							+"ORDER BY NOME";
				}
				 
		
				ps = conn.prepareStatement(SQL);
				
				rs = ps.executeQuery();
				
				listaItens = new ArrayList<Item>();
				
				while (rs.next()){
					Item item = new Item();
					
					item.setNome(rs.getString("nome"));
					item.setId(rs.getInt("ID"));
					item.setDescricao(rs.getString("descricao"));
					
					listaItens.add(item);
					
				}
				return listaItens;
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao obter lista de itens!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
		}		
		
		return listaItens = null;
	}

	
	

}
