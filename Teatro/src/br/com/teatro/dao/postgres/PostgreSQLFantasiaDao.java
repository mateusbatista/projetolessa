package br.com.teatro.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.teatro.bean.Fantasia;
import br.com.teatro.dao.FantasiaDao;

public class PostgreSQLFantasiaDao implements FantasiaDao {

	@Override
	public boolean novaFantasia(Fantasia novaFantasia) {
		
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "INSERT INTO fantasia (nome, id_tema, qnt_pecas, descricao, possui_perolas) values (?,?,?,?,?)";
				
				
				ps = conn.prepareStatement(SQL);
				
				ps.setString(1, novaFantasia.getNome());
				ps.setInt(2, novaFantasia.getIdTema());
				ps.setInt(3, novaFantasia.getQntPecas());
				ps.setString(4, novaFantasia.getDescricao());
				ps.setBoolean(5, novaFantasia.isPossuiPerolas());

				
				int rs = ps.executeUpdate();
				
				if (rs > 0){
					return true;
				}
				
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao gravar fantasia!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps);
		}		

		return false;
	}

	@Override
	public ArrayList<Fantasia> getListaFantasias() {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
	
		
		ArrayList<Fantasia> listaFantasias = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "SELECT * FROM FANTASIA";
		
				ps = conn.prepareStatement(SQL);
								
				rs = ps.executeQuery();
				
				listaFantasias = new ArrayList<Fantasia>();
				
				while (rs.next()){
					Fantasia fantasia = new Fantasia();
						

					listaFantasias.add(fantasia);
					
				}
				return listaFantasias;
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao obter lista de temas!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
		}		
		
		return listaFantasias = null;
	}

}
