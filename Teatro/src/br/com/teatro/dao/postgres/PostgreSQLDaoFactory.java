package br.com.teatro.dao.postgres;

import br.com.teatro.dao.CenarioDao;
import br.com.teatro.dao.DaoFactory;
import br.com.teatro.dao.FantasiaDao;
import br.com.teatro.dao.ItemDao;
import br.com.teatro.dao.MaquiagemDao;
import br.com.teatro.dao.TemaDao;
import br.com.teatro.dao.UsuarioDao;

public class PostgreSQLDaoFactory extends DaoFactory{

	@Override
	public TemaDao getTemaDao() {
		
		return new PostgreSQLTemaDao();
	}

	@Override
	public CenarioDao getCenarioDao() {
		return new PostgreSQLCenarioDao();
	}

	@Override
	public FantasiaDao getFantasiaDao() {
		return new PostgreSQLFantasiaDao();
	}

	@Override
	public MaquiagemDao getMaquiagemDao() {
		return new PostgreSQLMaquiagemDao();
	}
	
	public ItemDao getItemDao(){
		return new PostgreSQLItemDao();
	}

	@Override
	public UsuarioDao getUsuarioDao() {
		return new PostgreSQLUsuarioDao();
	}

}	


