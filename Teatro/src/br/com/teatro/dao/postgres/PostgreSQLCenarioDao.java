package br.com.teatro.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.teatro.bean.Cenario;
import br.com.teatro.dao.CenarioDao;

public class PostgreSQLCenarioDao implements CenarioDao {

	@Override
	public boolean novoCenario(Cenario novoCenario) {
		
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "INSERT INTO cenario (nome, id_tema, qnt_itens, descricao_itens) values (?,?,?,?)";
				
				
				ps = conn.prepareStatement(SQL);
				
				ps.setString(1, novoCenario.getNome());
				ps.setInt(2, novoCenario.getIdTema());
				ps.setInt(3, novoCenario.getQntItens());
				ps.setString(4, novoCenario.getDescricaoItens());

				
				int rs = ps.executeUpdate();
				
				if (rs > 0){
					return true;
				}
				
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao gravar cenario!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps);
		}		

		return false;
	}

	@Override
	public ArrayList<Cenario> getListaCenarios() {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
	
		
		ArrayList<Cenario> listaCenarios = null;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "SELECT * FROM TEMA";
		
				ps = conn.prepareStatement(SQL);
								
				rs = ps.executeQuery();
				
				listaCenarios = new ArrayList<Cenario>();
				
				while (rs.next()){
					Cenario cenario = new Cenario();
						

					listaCenarios.add(cenario);
					
				}
				return listaCenarios;
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao obter lista de temas!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
		}		
		
		return listaCenarios = null;
	}

}
