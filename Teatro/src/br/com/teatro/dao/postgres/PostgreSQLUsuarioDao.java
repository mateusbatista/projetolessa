package br.com.teatro.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.teatro.bean.Usuario;
import br.com.teatro.dao.UsuarioDao;

public class PostgreSQLUsuarioDao implements UsuarioDao {

	public Usuario getUsuario(Usuario usuario) {
	
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "SELECT * FROM USUARIO"
						+ " WHERE LOGIN=? AND SENHA= ?";
		
				ps = conn.prepareStatement(SQL);
				
				ps.setString(1, usuario.getLogin());
				ps.setString(2, usuario.getSenha());
				
				rs = ps.executeQuery();
				
				while (rs.next()){
					usuario.setIdUsuario(rs.getInt("id_usuario"));
					return usuario;
				}
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao obter informações!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
		}		
		
		return usuario = null;
	}

	@Override
	public int adicionaUsuario(Usuario novoUsuario) {
		
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		int idUsuario = 0;
		
		try {
			conn = PostgreSQLConnectionFactory.getConexao();

			if (conn != null) {
			
				String SQL = "INSERT INTO usuario (login, senha, id_cliente)"
						+ " VALUES (?,?,?) RETURNING id_usuario ";
				
				
				ps = conn.prepareStatement(SQL);
				
				ps.setString(1, novoUsuario.getLogin());
				ps.setString(2, novoUsuario.getSenha());
				ps.setInt(3, novoUsuario.getIdCliente());

				rs = ps.executeQuery();
				
				if (rs.next()){
					idUsuario = rs.getInt("id_usuario");
				}
				
				return idUsuario;
			}
			
		} catch (SQLException e) {
			System.out.println("Erro ao gravar usu�rio!");
			e.printStackTrace();
		} finally {
			PostgreSQLConnectionFactory.fechaRecursos(conn, ps, rs);
		}		
		
		return 0;
	}

	@Override
	public int alteraUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		return 0;
	}

}
