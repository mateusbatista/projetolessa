package br.com.teatro.dao;

import java.util.ArrayList;

import br.com.teatro.bean.Item;

public interface ItemDao {
	public ArrayList<Item> getListaItens();
	public ArrayList<Item> getListaItens(int idTema);
	public ArrayList<Item> getListaItensTipo(int idTipo);

}
