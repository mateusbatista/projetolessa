package br.com.teatro.dao;

import java.util.ArrayList;

import br.com.teatro.bean.Tema;

public interface TemaDao {
	
	public boolean novoTema(Tema novoTema);
	
	public ArrayList<Tema> getListaTemas();
}
