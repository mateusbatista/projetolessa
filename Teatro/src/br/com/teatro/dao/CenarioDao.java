package br.com.teatro.dao;

import java.util.ArrayList;

import br.com.teatro.bean.Cenario;

public interface CenarioDao {
	public boolean novoCenario(Cenario novoCenario);
	public ArrayList<Cenario> getListaCenarios();
}
