package br.com.teatro.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AutorizadorInterceptor extends HandlerInterceptorAdapter {
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller) throws Exception {

		      String uri = request.getRequestURI();
		      
		      System.out.println(uri);
		      if(uri.endsWith("login") || uri.endsWith("registro")||uri.endsWith("registrar") || uri.endsWith("efetuaLogin") ||  uri.contains("assets")){
		        return true;
		      }

		      if(request.getSession().getAttribute("usuarioLogado") != null) {
		    	 System.out.println("Usua�rio logado: "+request.getSession().getAttribute("usuarioLogado"));
		        return true;
		      }

		      response.sendRedirect("login");
		      return false;
		  }
}
