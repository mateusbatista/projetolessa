<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
      <meta charset="ISO-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Cadastro Tema</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index">Teatro</a> 
            </div>
            	<div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"><a href="logout" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                 <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="assets/img/teatro_mask.png" class="user-image img-responsive"/>
					</li>
									                   
                    <li>
                        <a href="#"><i class="fa fa-pencil-square-o fa-3x"></i> Cadastros<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="tema">Tema</a>
                            </li>
                            <li>
                                <a href="cenario">Cen�rio</a>
                            </li>
                            <li>
                                <a href="#">Fantasia<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="fantasia">Fantasia</a>
                                    </li>
                                    <li>
                                        <a href="maquiagem">Maquiagem</a>
                                    </li>
                                </ul>
                               
                            </li>                         
                        </ul>
                      </li>
                      
                      <li>
                        <a href="#"><i class="fa fa-list-alt fa-3x"></i> Relat�rios<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="relatorioTema">Por tema</a>
                            </li>
                            <li>
                                <a href="relatorioTipo">Por tipo</a>
                            </li>                         
                        </ul>
                      </li> 
                      	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row">
					<div class="col-md-12">
						<h2>Cadastro de tema</h2>
						<h5>Cadastre os temas a serem vinculados com os cen�rios e
							fantasias.</h5>

					</div>
				</div>
				<!-- /. ROW  -->
				<hr />

				<!--  Inicio do form -->

				<div class="row">
					<div class="col-md-12">
						<!-- Form Elements -->
						<div class="panel panel-default">
							<div class="panel-heading">Cadastro</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<form role="form" action="novoTema" method="post">
											<div class="form-group">
												<label>Nome</label> <input id="nome" name="nome" class="form-control" />
											</div>

											<div class="form-group">
												<label>Descri��o</label>
												<textarea id="descricao" name="descricao" class="form-control" rows="3"></textarea>
											</div>

											<button type="submit" class="btn btn-primary">Salvar</button>
											<button type="reset" class="btn  btn-default">Limpar</button>

										</form>
										<br />
									</div>

								</div>
							</div>
						</div>
						<!-- End Form Elements -->
					</div>
				</div>
				<!-- /. ROW  -->
			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
