<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
      <meta charset="ISO-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Titulo</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-cls-top " role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index">Teatro</a>
			</div>
			<div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"><a href="logout" class="btn btn-danger square-btn-adjust">Logout</a> </div>
		</nav>
		<!-- /. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">
					<li class="text-center"><img src="assets/img/teatro_mask.png"
						class="user-image img-responsive" /></li>

					<li><a href="#"><i class="fa fa-pencil-square-o fa-3x"></i>
							Cadastros<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="tema">Tema</a></li>
							<li><a href="cenario">Cen�rio</a></li>
							<li><a href="#">Fantasia<span class="fa arrow"></span></a>
								<ul class="nav nav-third-level">
									<li><a href="fantasia">Fantasia</a></li>
									<li><a href="maquiagem">Maquiagem</a></li>
								</ul></li>
						</ul></li>

					<li><a href="#"><i class="fa fa-list-alt fa-3x"></i>
							Relat�rios<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="relatorioTema">Por tema</a></li>
							<li><a href="relatorioTipo">Por tipo</a></li>
						</ul></li>

				</ul>

			</div>

		</nav>
		<!-- /. NAV SIDE  -->
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row">
					<div class="col-md-12">
						<h2>Bem vindo!</h2>
					</div>
				</div>
				<!-- /. ROW  -->
				<hr />
				
				<!-- Boxes -->
				<div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <a class="fa fa-magic" href="tema"></a>
                </span>
                <div class="text-box" >
                    <p class="main-text">Tema</p>
                </div>
             </div>
		     </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <a class="fa fa-picture-o" href="cenario"></a>
                </span>
                <div class="text-box" >
                    <p class="main-text">Cen�rio</p>
                </div>
             </div>
		     </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                    <a class="fa fa-github-alt" href="fantasia"></a>
                </span>
                <div class="text-box" >
                    <p class="main-text">Fantasia</p>
                </div>
             </div>
		     </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <a class="fa fa-tint" href="maquiagem"></a>
                </span>
                <div class="text-box" >
                    <p class="main-text">Maquiagem</p>
                </div>
             </div>
		     </div>
			</div>
				
				<!-- fim boxes -->
			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
	</div>
	<!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
