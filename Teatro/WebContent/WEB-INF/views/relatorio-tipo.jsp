<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Relatório por Tipo</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
       <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
   
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
   <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	
	
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index">Teatro</a> 
            </div>
            	<div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"><a href="logout" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                 <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="assets/img/teatro_mask.png" class="user-image img-responsive"/>
					</li>
									                   
                    <li>
                        <a href="#"><i class="fa fa-pencil-square-o fa-3x"></i> Cadastros<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="tema">Tema</a>
                            </li>
                            <li>
                                <a href="cenario">Cenário</a>
                            </li>
                            <li>
                                <a href="#">Fantasia<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="fantasia">Fantasia</a>
                                    </li>
                                    <li>
                                        <a href="maquiagem">Maquiagem</a>
                                    </li>
                                </ul> 
                            </li>                          
                        </ul>
                      </li>
                      
                      <li>
                        <a href="#"><i class="fa fa-list-alt fa-3x"></i> Relatórios<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="relatorioTema">Por tema</a>
                            </li>
                            <li>
                                <a href="relatorioTipo">Por tipo</a>
                            </li>                         
                        </ul>
                      </li> 
                      	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row">
					<div class="col-md-12">
						<h2>Relatório de itens por tipo</h2>
						<h5>Selecione um tipo para gerar o relatório.</h5>

					</div>
				</div>
				<!-- /. ROW  -->
				<hr />

				<!--  Inicio do form -->

				<div class="row">
					<div class="col-md-12">
						<!-- Form Elements -->
						<div class="panel panel-default">
							<div class="panel-heading">Seleção</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<form role="form" action="relatorioTipo" method="post">

											<div class="form-group">
												<label>Tipo</label> <select id="idTipo" name="idTipo" class="form-control">
													<!-- Entra for each -->
														<option value="2">Cenário</option>
														<option value="1">Fantasia</option>
														<option value="0">Todos</option>
												</select>
											</div>

											<button type="submit" class="btn btn-primary">Gerar</button>
										</form>
										<br />
									</div>
								</div>
							</div>
						</div>
						<!-- End Form Elements -->
						
						<!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Relatório
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="tabelaItens">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>NOME</th>
                                            <th>Descrição</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${listaItens}" var="item">
	                                	<tr>
	                                		<td>${item.id}</td> 
	                                		<td>${item.nome}</td>
	                                		<td>${item.descricao}</td>
	                                	</tr>           		
	                                </c:forEach>                                     
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
						
						
						
					</div>
				</div>
				<!-- /. ROW  -->
			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
         <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#tabelaItens').dataTable();
            });
    </script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
