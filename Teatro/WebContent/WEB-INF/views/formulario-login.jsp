<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

<title>Login</title>
</head>
<body>

<div class="container">

<div class="row" style="margin-top:20px">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		<form role="form" action="efetuaLogin" method="post">
			<fieldset>
				<h2>Por favor entre</h2>
				<hr class="colorgraph">
				<div class="form-group">
                    <input type="text" name="login" id="login" class="form-control input-lg" placeholder="Login">
				</div>
				<div class="form-group">
                    <input type="password" name="senha" id="senha" class="form-control input-lg" placeholder="Senha">
				</div>
				<!-- 
				<span class="button-checkbox">
					<button type="button" class="hidden" data-color="info">Lembre-me</button>
					<input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
					<a href="" class="btn btn-link pull-right">Esqueceu sua senha?</a>
				</span>
				 -->
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6">
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Entrar">
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>

</div>

</body>
</html>